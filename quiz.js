// 1.
function getFirstAndLastChars(givenString)
{
    var first = givenString[0];
    var last = givenString[givenString.length - 1];
    return first + last;
}
var chars = getFirstAndLastChars("Liam");
console.log(chars);


// 2.
function generateHtmlTable(fromLowValue, toHighValue)
{
    var table = "<table border='1'>";
    for (var index = fromLowValue; index <= toHighValue; index++)
    {
        table += "<tr>";
        table += "<td>" + index + "</td>";
        table += "</tr>";
    }
    table += "</table>";
    return table;
}
var htmlTable = generateHtmlTable(5, 20);
document.write(htmlTable);