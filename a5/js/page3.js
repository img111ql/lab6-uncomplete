function isNumeric(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
  }
  

// VALIDATE THE BMI FORM

var isCorrect = true;

var form = document.getElementById("bmiForm");
form.style.border = "2px solid red";
var button = form.getElementsByTagName('button')[0];

var errorArea = document.getElementById('bmiForm-errors');
errorArea.style.color = 'red';
errorArea.innerHTML = "";

// validate weight :: must be number, positive, larger than one kg, smaller than 1000
var weightElement = document.getElementById('weight');

var weight = 1 * weightElement.value;
console.log(weight);
if (!isNumeric(weight) || (weight < 1) || (weight > 1000))
{
    errorArea.innerHTML += "Weight must be a number in range from 1-1000";
    isCorrect = false;
}

// validate height :: must be number, positive, larger than one kg, smaller than 1000
var heightElement = document.getElementById('height');

var height = 1 * heightElement.value;
console.log(height);
if (!isNumeric(height) || (height < 1) || (height > 100))
{
    errorArea.innerHTML += "Height must be a number in range from 1-100";
    isCorrect = false;
}



// // attach event handler to each td element - Dumb Move
// var tds = document.getElementsByTagName('td');
// console.log(tds.length);

// for (var index = 0; index < tds.length; index++) 
// {
//     tds[index].addEventListener('click', function(){
//         var element = event.target;
//         console.log(element.textContent);
//     })
// }

// // use Event Delegation to get content of each TD element - Smart Move
// var table = document.getElementById('mytable');
// table.style.border = "4px blue outset";
// table.addEventListener('click', function(){
//     var td = event.target;
//     console.log(td.textContent);
// })


// let us try to DELEGATE to the BODY element
// var body = document.getElementsByTagName('body')[0];
// console.log(body);
// body.addEventListener('click', function(event){
//     var td = event.target;
//     console.log(td.textContent);
// })



// // let us find the calculate button

// var form = document.getElementById("bmiForm");
// form.style.border = "2px solid red";

// var buttons = document.getElementsByTagName('button');
// var button = buttons[2];
// button.style.border = "2px solid green";

// // show alert box when we click

// button.addEventListener('click', function () {
//     console.log('You Clicked');
// })